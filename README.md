## SVEPM 2021 - WS5 : Extraction of medical terms from non-structured (textual) datafrom online news and social media
Link to the workshop: [Wednesday 24th March, 14.00-17.00](https://www.svepm2021.org/upload/pdf/SVEPM2021_WS5.pdf).

Introduction of Natural Language Processing (NLP) methods applied to health domain : an overview of terminology extraction from online news and social network (Twitter).

### How to use this repository
You can launch a jupyter notebook thank to [mybinder](https://mybinder.org/) : [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.irstea.fr%2Fremy.decoupes%2Fsvepm2021-ws5-textmining/HEAD)

![quick start](readme_ressources/svepm_lauch_binder.gif)

### Authors
[UMR TÉTIS](https://www.umr-tetis.fr/)

### License
This code is provided under the [CeCILL-B](https://cecill.info/licences/Licence_CeCILL-B_V1-en.html) free software license agreement.


